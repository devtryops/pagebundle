const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const fs = require('fs');
const path = require('path');
// const jimp = require('jimp');
const sharp = require('sharp');

const host = "0.0.0.0";
const port = 8042;
const app = express();
const basePath = __dirname + "/TestPages";
const bundleDB = {rawList:[]};
const basePageTemplatePath = __dirname + "/pageTemplates";

app.use(express.static('frontend'));
app.use(express.static('TestPages'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.listen(port, host);

const readBuild = () => {
	let files = fs.readdirSync(basePath);
	if(files||false) {
		// console.dir(files);
		for(let f=0,fl=files.length;f < fl; f++) {
			let state = fs.statSync(basePath+"/"+files[f]);
			if(state.isDirectory()||false) {
				try {
					let stepDown = fs.statSync(basePath+"/"+files[f]+'/index.json');
					if(stepDown.isFile()||false) {
						let fromFileJson = fs.readFileSync(basePath+"/"+files[f]+'/index.json');
						let neoObj = JSON.parse(fromFileJson);
						neoObj.basePath = encodeURIComponent(files[f]);
						neoObj.coverImg = neoObj.pages[0].image;
						// neoObj.folder = encodeURIComponent(files[f]);
						bundleDB.rawList.push(neoObj);
					}
				} catch(e) {
					console.log("no index");
				}
			}
		}
		// console.dir(bundleDB);
		console.log(`Folder count: ${files.length} && Database entrys: ${bundleDB.rawList.length}`);
	} else {
		console.log("err: some on "+basePath);
	}
};

readBuild();


const hbs = exphbs.create({
  helpers: {
    foo: function() {return 'FOO!!1!!1!!'},
    bar: function() {return 'HOLYBARTENDA'},
    korm: function(nVal) {
      if(nVal>=1000000) {
        let fMio = nVal/1000000;
        return parseInt(fMio)+"M "+parseInt((fMio-Math.trunc(fMio))*1000)+"K";
      } else if(nVal>=1000) {
        let fKilo = nVal/1000;
        return parseInt(fKilo)+"K "+Math.trunc( (fKilo-Math.trunc(fKilo))*1000 );
        // return fKilo+" | "+Math.trunc(fKilo)+" | "+parseInt(fKilo);
      } else {
        return nVal;
      }
    },
    timeSince: function(timestamp) {
      let localTime = new Date()*1;
      let timeDiff = localTime - timestamp;
      seconds = Number(timeDiff/1000);
      var d = Math.floor(seconds / (3600*24));
      var h = Math.floor(seconds % (3600*24) / 3600);
      var m = Math.floor(seconds % 3600 / 60);
      var s = Math.floor(seconds % 60);

      var dDisplay = d > 0 ? d + "D " : "";
      var hDisplay = h > 0 ? h + "H " : "";
      var mDisplay = m > 0 ? m + "M " : "";
      var sDisplay = s > 0 ? s + "S " : "";
      return dDisplay + hDisplay + mDisplay + sDisplay;
    }
  }
});

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

app.get('/view/main/', async (req, res, next) => {
  res.render('mainView', {
    bundleList: bundleDB.rawList,

    helpers: {
      fooz: () => { return 'foobazz';}
    }
  });
});

const getJsonFromFolder = (folder) => {
	let tmpBundle = {pages:[]};
	let rawJSON = fs.readFileSync(basePath+"/"+folder+'/index.json');
	if(rawJSON||false) {
		tmpBundle = JSON.parse(rawJSON);
	}
	return tmpBundle;
};

const saveJsonForFolder = (folder,jsonObj) => {
	// console.log(`base ${basePath} and folder: ${folder}`);
	let reWrite = fs.writeFileSync(basePath+"/"+folder+'/index.json',JSON.stringify(jsonObj));
	return reWrite;
};

const getPageByFolder = async (folder) => {
	return bundleDB.rawList.map( e => { if(e.basePath === encodeURIComponent(folder)) return e; })[0];
};

app.get('/view/bundle/:folder', async (req, res, next) => {
	// console.log(req.params.folder);
	let knowTest = bundleDB.rawList.map( e => { if(e.basePath == encodeURIComponent(req.params.folder)) return e; });
	if(knowTest.length<=0) {
		console.dir(knowTest);
		res.send("no");
	} else {
		let jsonBundle = getJsonFromFolder(req.params.folder);
		res.render('bundleView', {
			bundle: jsonBundle,
			pages: jsonBundle.pages,
			basePath: encodeURIComponent(req.params.folder),
			helpers: {
				dooz: () => { return 'donz';}
			}
		});
	}
});

app.get('/json/folder/list', async (req, res, next) => {
	res.json(bundleDB.rawList.map(e => e.basePath));
});

app.get('/json/bundle/:folder', async (req, res, next) => {
	let knowTest = bundleDB.rawList.map( e => { if(e.basePath == encodeURIComponent(req.params.folder)) return e; });
	if(knowTest.length<=0) {
		console.dir(knowTest);
		res.json({err:"no way to do it !!1!1!!11!"});
	} else {
		res.json(getJsonFromFolder(req.params.folder));
	}
});

app.post('/json/bundle/:folder', async (req, res, next) => {
	let folder = req.params.folder;
	let knowTest = bundleDB.rawList.map( e => { if(e.basePath == encodeURIComponent(folder)) return e; });
	// console.dir(req.body.bundle);
	// console.log('knowing test result '+folder);
	console.dir(knowTest);
	if(knowTest.length<=0) {
		res.json({err:'that not the folder you are looking for.'});
	} else {
		// 
		// ToDO:
		// 
		// some timing error, without console.log bundle is undefined
		let bundle = getPageByFolder(folder);
		console.dir(bundleDB.rawList.map( e => { if(e.basePath === encodeURIComponent(folder)) return e; })[0]);
		bundle.pages = req.body.bundle.pages;
		let howItsGoing = saveJsonForFolder(req.params.folder, bundle);
		res.json({msg:'well done',howItsGoing});
	}
});

app.get('/json/template/list', async (req, res, next) => {
	let files = fs.readdirSync(basePageTemplatePath);
	console.dir(files);
	if(files||false) {
		res.json({filelist:files});	
	} else {
		res.json({err:"no files here"});
	}
});

app.get('/json/template/:name', async (req, res, next) => {
	let filepath = basePageTemplatePath+'/'+req.params.name;
	// console.log(`template name ${filepath}`);
	let state = fs.statSync(filepath);
	if(state.isFile()) {
		let fileRaw = fs.readFileSync(filepath);
		if(fileRaw||false) {
			res.json(JSON.parse(fileRaw));
		} else {
			res.json({err:'file is empty',filepath,filename:req.params.name});
		}
	}	else {
		res.json({err:"no file here",filepath,filename:req.params.name});
	}
});

app.post('/json/template/:name', async (req, res, next) => {
	// console.log(`template name ${req.params.name}`);
	res.json({err:"no code yet"});
});


app.get('/crop/:folder/:imgfile', async (req, res, next) => {
	// req.setTimeout(5000000);
	// console.log(`Test not there ${req.query.xxyy}`);
	let x1 = (parseInt(req.query.x1)>=0||false)? parseInt(req.query.x1) : false;
	let y1 = (parseInt(req.query.y1)>=0||false)? parseInt(req.query.y1) : false;
	if( x1===false || y1===false ) {
		res.json({err:`missing x1:${x1} or y1:${y1} valuse`});
	}
	let x2 = (parseInt(req.query.x2)>=0||false)? parseInt(req.query.x2) : false;
	let y2 = (parseInt(req.query.y2)>=0||false)? parseInt(req.query.y2) : false;
	if( x2===false || y2===false ) {
		res.json({err:"missing x2 or y2 valuse"});
	}
	// const image = await jimp.read(`${basePath}/${req.params.folder}/${req.params.imgfile}`);
	// await image.crop(x1,y1,x2-x1,y2-y1).getBuffer(jimp.MIME_JPEG, (err, buffer) => {
	// 	if(err||false) {
	// 		res.json({err:"no image file we can show"});
	// 	} else {
	// 		res.set("Content-Type", jimp.MIME_JPEG);
	// 		res.send(buffer);
	// 	}
	// });

	let filename = `${basePath}\/${req.params.folder}\/${req.params.imgfile}`;
	// let width = x2 - x1;
	// let height = y2 - y1;
	// let top = y1;
	// let left = x1;
	let sharpRet = await sharp(filename).extract({ width: x2-x1, height: y2-y1, top: y1, left: x1}).pipe(res);
	// console.log(sharpRet);
});

console.log(`Running on http://${host}:${port}`);

module.exports = app;