const pro = require('commander');
const fs = require('fs');
const path = require('path');
const sizeOfImg = require('image-size');

const baseFolder = './TestPages/'

const listFolder = '../Issue#2/';


const oldSupidTest = () => {
	let out = {
		list: [],
		title: "Ausgabe #00",
		date: "08.02.2020",
		autor: "hachette",
		creater: "Programm"
	}
	fs.readdir(listFolder, (err, files) => {
		out.list = [{inx:0,err:"no page 0"}];
		files.forEach(file => {
			if(file.indexOf('png')>-1) {
				let ext = path.extname(file);
				let base = path.basename(file,ext);
				let inx = parseInt(file.match(/[0-9]+\./)[0]);
				let pageTitle = "page title";
				let frames = [];
				out.list[inx] = {file,ext,base,inx,pageTitle,frames};
			}
		});
		fs.writeFileSync('testIssue#2.json', JSON.stringify(out));
	});
};

const readDirPath = (nextLvlPath) => {
	let files = fs.readdirSync(baseFolder+nextLvlPath);
	if(files||false) {
		return files;
	} else {
		return [];
	}
};

const genJSONfile = (fileList,nextLvlPath,bundleConf) => {
	let out = {
		pages: []
	};

	out.title = (bundleConf.title||false)? bundleConf.title : "Ausgabe #00";
	out.date = (bundleConf.date||false)? bundleConf.date :  "08.02.2020";
	out.autor =  (bundleConf.autor||false)? bundleConf.autor : "god";
	out.creator = (bundleConf.creator||false)? bundleConf.creator : "Programm";
	

	for(let f=0,fl=fileList.length; f < fl; f++) {
		if(!fileList[f].match(/jpg|png|tif/g)) continue;
		let page = {frames:[]};
		page.image = fileList[f];
		page.title = 'no title yet';
		page.index = parseInt(fileList[f].match(/[0-9]+\./));
		page.dimensions = sizeOfImg(baseFolder+nextLvlPath+'/'+fileList[f]);
		page.frames.push({index:0,x1:0,y1:0,x2:page.dimensions.width,y2:page.dimensions.height,title:'no page title yet'});
		out.pages.push(page);
	}

	// return {"no":"data","yet":"null"};
	return out;
};


// commander simple setup
pro
 .option('-d, --dir <pathtoimg>', 'set directory to read from', 'Issue#1')
 .option('-o, --file <outfile>', 'filename to export to', 'index.json')
 .option('-t, --title <bundletitle>', 'title of the bundle', 'Ausgabe #000')
 .option('--date <pubdate>', 'publish date', '08.02.2020')
 .option('--autor <autor>', 'who create that', 'god')
 .option('--creator <ofthebundle>', 'who put this together', 'Programm')
 .parse(process.argv)

if(pro.dir != false) {
	let listOfFiles = readDirPath(pro.dir);
	
	if(listOfFiles||false && listOfFiles[0]||false) {
		
		let conf = {};
		conf.title = pro.title;
		conf.date = pro.date;
		conf.autor = pro.autor;
		conf.creator = pro.creator;
		// console.log('re JSON:');
		// console.dir(reJson);
		
		let reJson = genJSONfile(listOfFiles,pro.dir,conf);


		let reFileWrite = fs.writeFileSync(baseFolder+pro.dir+'/'+pro.file, JSON.stringify(reJson));
		console.dir(reFileWrite);
	} else {
		console.log(`no files found in ${baseFolder}${pro.dir}`);
	}
} else {
	console.log(`no directory given`);
}