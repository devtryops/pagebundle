let pageBundle = {pages:[],folder:'',folderList:[]};
let templateCache = {};
let cacheErrCount = 0;


// 
// Template load and render
// 
const loadTemplate = (tempId, callback) => {
	superagent
	 .get('/tpl/'+tempId+'.handlebars')
	 .then(res => {
	 		cacheErrCount = -1;
	 		templateCache[tempId] = res.text;
	 		callback(res.text);
	 })
	 .catch(err => {
	 		cacheErrCount++
	 		if(cacheErrCount <= 10) {
		 		console.log(err.message);
	      console.dir(err.response);
	    }
	 });
};

const renderTemplate = (tempId, dataSet, target) => {
	if(templateCache[tempId]||false) {
		const template = templateCache[tempId];
		const comTempl = Handlebars.compile(template);

		const html = comTempl(dataSet);
		document.getElementById(target).innerHTML = html;
	} else {
		loadTemplate(tempId, () => { renderTemplate(tempId, dataSet, target); });
	}
};
// 
// 


// 
// load save state to localstorage
// 
const saveState = () => {
	let strState = JSON.stringify(state);
	localStorage.setItem('pfmstate', strState);
};

const loadState = () => {
	let loState = localStorage.getItem('pfmstate');
	if(loState||false) {
		let tmpState = Object.assign(state, JSON.parse(loState));
		state = tmpState;
	} else {
		console.log('error while loading the state from');
		saveState();
	}
};

// 
// 


// 
// global element manipulation 
// 

const selectFolderToView = () => {
	let selList = document.getElementById('folderSelectTarget');
	let newURL = window.location.protocol + '//' + window.location.host + window.location.pathname + '?folder='+selList.options[selList.selectedIndex].text;
	// window.location.href = newURL;
	console.log(newURL);
	window.location.replace(newURL);
};

// 
// 


// 
// tags sorting filter or views of tags
// 

const getTagCloudFromBundle = () => {
	let tagC = {};
	for(let p=0,pl=pageBundle.pages.length; p < pl; p++) {
		for(let f=0,fl=pageBundle.pages[p].frames.length; f < fl; f++) {
			if( !(pageBundle.pages[p].frames[f].tags||false) ) continue;
			for(let t=0,tl=pageBundle.pages[p].frames[f].tags.length; t < tl; t++) {
				if(tagC[pageBundle.pages[p].frames[f].tags[t]]||false) {
					tagC[pageBundle.pages[p].frames[f].tags[t]].count++;
					tagC[pageBundle.pages[p].frames[f].tags[t]].targets.push(`${p}-${f}`);
				} else {
					tagC[pageBundle.pages[p].frames[f].tags[t]] = { count: 1, targets: [`${p}-${f}`] };
				}
			}
		}
	}
	return tagC;
};



// 
// 


// 
// 
// 
