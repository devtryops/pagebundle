// ***************************************************************************
// 
//            Setup Interact Lib
// 
// ***************************************************************************
// target elements with the "draggable" class
interact('.draggable')
  .draggable({
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'parent',
        endOnly: true
      })
    ],
    // enable autoScroll
    autoScroll: true,

    listeners: {
      // call this function on every dragmove event
      move: dragMoveListener,

      // call this function on every dragend event
      // end (event) {
      //   var textEl = event.target.querySelector('p')

      //   textEl && (textEl.textContent =
      //     'moved a distance of ' +
      //     (Math.sqrt(Math.pow(event.pageX - event.x0, 2) +
      //                Math.pow(event.pageY - event.y0, 2) | 0))
      //       .toFixed(2) + 'px')
      // }
    }
  })
  .resizable({
    edges: {
      top: true,
      left: true,
      bottom: true,
      right: true
    }
  })
  .on('resizemove', event => {
    let { x, y } = event.target.dataset

    x = parseFloat(x) || 0
    y = parseFloat(y) || 0

    Object.assign(event.target.style, {
      width: `${event.rect.width}px`,
      height: `${event.rect.height}px`,
      transform: `translate(${event.deltaRect.left}px, ${event.deltaRect.top}px)`
    })

    Object.assign(event.target.dataset, { x, y })
  })

function dragMoveListener (event) {
  var target = event.target
  // keep the dragged position in the data-x/data-y attributes
  var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
  var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

  // translate the element
  target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)'

  // update the posiion attributes
  target.setAttribute('data-x', x)
  target.setAttribute('data-y', y)
}

// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener
// ***************************************************************************
// 
//            END Setup Interact Lib
// 
// ***************************************************************************




// on page btn handler

const selectSideView = (selectType) => {
  console.log(`clicked selected type ${selectType}`);
  if(selectType==='tags') {
    let tagObj = getTagCloudFromBundle();
    state.tagCloud = [];
    for(let n in tagObj) {
      state.tagCloud.push({name:n,count:tagObj[n].count,targets: tagObj[n].targets});
    }

    renderTemplate('tagSideView', {list:state.tagCloud}, 'sideBarTarget');
  }
};

const showAllTagFrames = (tagToShow) => {
  console.log(`clicked tag button ${tagToShow}`);
  let targetNo = state.tagCloud.findIndex( e => {return e.name===tagToShow;} );
  let targetList = state.tagCloud[targetNo].targets;
  let imgList = [];
  for(let t=0,tl=targetList.length; t < tl; t++) {
    let pInx = parseInt(targetList[t].split('-')[0]);
    let fInx = parseInt(targetList[t].split('-')[1]);
    let pageImg = '/crop/'+state.folder+'/'+pageBundle.pages[pInx].image;
    let frame = pageBundle.pages[pInx].frames[fInx];
    let cropQy = `?x1=${frame.x1}&y1=${frame.y1}&x2=${frame.x2}&y2=${frame.y2}`;
    console.log();
    imgList.push({src:pageImg+cropQy,frame});
  }
  console.dir(imgList);

  renderTemplate('tagViewWindow', {imglist:imgList}, 'viewWindowTarget');

};