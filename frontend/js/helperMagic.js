// some helper for handle data and stuff like that
// ....

const getPageByIndex = (indexNo) => {
	return pageBundle.pages.filter( e => { return e.index===parseInt(indexNo) } )[0];
};

const getPageArrayIndex = (indexNo) => {
	return pageBundle.pages.findIndex( e => { if(e.index===parseInt(indexNo)) return true; } );
};

const getFrameByIndex = (frames,index) => {
	return frames.filter( e => { return e.index === parseInt(index) })[0];
};

const getFrameArrayIndex = (frames,index) => {
	return frames.findIndex( e => { if(e.index===parseInt(index)) return true; } );
};

const getActiveBoxMinMaxPos = (list,fac) => {
	let aBox = list.filter( e => { return e.active===true; });
	let max = {x:0,y:0};
	let min = {x:9999999,y:9999999};
	for(let b=0,bl=aBox.length; b < bl; b++) {
		if(aBox[b].x1<min.x) min.x = aBox[b].x1/fac;
		if(aBox[b].x1>max.x) max.x = aBox[b].x1/fac;
		if(aBox[b].x2<min.x) min.x = aBox[b].x2/fac;
		if(aBox[b].x2>max.x) max.x = aBox[b].x2/fac;
		if(aBox[b].y1<min.y) min.y = aBox[b].y1/fac;
		if(aBox[b].y1>max.y) max.y = aBox[b].y1/fac;
		if(aBox[b].y2<min.y) min.y = aBox[b].y2/fac;
		if(aBox[b].y2>max.y) max.y = aBox[b].y2/fac;
	}
	return {min,max};
};

const getListOfPageIndex = () => {
	let allIndex = [];
	for(let p=0,pl=pageBundle.pages.length; p < pl; p++) {
		allIndex.push(pageBundle.pages[p].index);
	}
	allIndex.sort( (a,b) => { return (a-b); });
	return allIndex;
};

const getNextIndexInPages = (startIndex) => {
	let allIndex = getListOfPageIndex();
	let inList = allIndex.indexOf(parseInt(startIndex));
	if(inList===allIndex.length-1) {
		return allIndex[0];
	} else {
		return allIndex[inList+1];
	}
};

const getPrevIndexInPages = (startIndex) => {
	let allIndex = getListOfPageIndex();
	let inList = allIndex.indexOf(parseInt(startIndex));
	if(inList===0) {
		return allIndex[allIndex.length-1];
	} else {
		return allIndex[inList-1];
	}
};

const genTagCloud = () => {
	for(let p=0,pl=pageBundle.pages.length; p < pl; p++) {
	  if( !(pageBundle.pages[p].frames||false) ) continue;
	  for(let f=0,fl=pageBundle.pages[p].frames.length; f < fl; f++) {
	    if( !(pageBundle.pages[p].frames[f].tags||false) ) continue;
		    for(let t=0,tl=pageBundle.pages[p].frames[f].tags.length; t < tl; t++) {
		     if(tagCloud.indexOf(pageBundle.pages[p].frames[f].tags[t])===-1) {
	    	   tagCloud.push(pageBundle.pages[p].frames[f].tags[t]);
	  	  }
	    }
	  }
	}
};

const tagBackSearch = (searchString) => {
	let lowStr = searchString.toLocaleLowerCase();
	return tagCloud.filter( e => { return (e.toLocaleLowerCase().indexOf(lowStr)!=-1); } );
};

const fillEditPageData = (pageData) => {
	state.editPageIndex = pageData.index;
	let formEl = document.getElementById('editForm');
	formEl.style.display = "inline-block";
	document.getElementById('backToLastImage').style.display = 'inline-block';
	formEl.querySelector('#mainTitleInput').value = pageData.title;
	renderTemplate('frameList',pageData,"frameListSection");
};

const addFrameToPage = (conf) => {
	// console.log(state.editPageIndex);
	// console.dir(conf);
	if(!(conf||false)) conf = {};
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	let lastFrame = pageBundle.pages[arrIndex].frames.length;
	// let nIndex = (conf.index||false)? conf.index : lastFrame;
	let nTitle = (conf.title||false)? conf.title : 'no page title yey';
	let nTags = (conf.tags||false)? conf.tags : [];
	let nX1 = (conf.x1||false)? conf.x1 : 50;
	let nY1 = (conf.y1||false)? conf.y1 : 50;
	let nX2 = (conf.x2||false)? conf.x2 : 750;
	let nY2 = (conf.y2||false)? conf.y2 : 750;
	
	pageBundle.pages[arrIndex].frames[lastFrame] = {
                    "index": lastFrame,
                    "title": nTitle,
                    "tags": nTags,
                    "x1": nX1,
                    "y1": nY1,
                    "x2": nX2,
                    "y2": nY2,
                };
	renderTemplate('frameList',pageBundle.pages[arrIndex],"frameListSection");
	framesToBoxes();
	drawCanvas();
	return lastFrame;
};

const saveFullPage = () => {
	console.log(state.editPageIndex);
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	console.dir(pageBundle.pages[arrIndex]);
	superagent
   .post('/json/bundle/'+state.folder)
   .send({bundle:pageBundle})
   .then(res => {
      console.dir(res);
   })
   .catch(err => {
      // err.message, err.response
      console.log(err.message);
      console.dir(err.response);
   });
};

const showHideFrameEdit = (index,showorhide) => {
	let btnSave = document.getElementById('frameBtnSaveOne_'+index);
	let btnCancel = document.getElementById('frameBtnCancelOne_'+index);
	if(showorhide==='show') {
		btnSave.style.display = "inline-block";
		btnCancel.style.display = "inline-block";	
	} else if(showorhide==='hide') {
		btnSave.style.display = "none";
		btnCancel.style.display = "none";
	}
	
};

const someFrameWasEdited = (index) => {
	showHideFrameEdit(index,'show');
};


const saveOneFrame = (index) => {
	console.log('some save code here');
	showHideFrameEdit(index,'hide');
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, index);
	page.frames[frameIndex].title = document.getElementById('frameInputTitle_'+index).value;
	let boxz = getFrameByIndex(state.ctx.box,index);
	
	page.frames[frameIndex].x1 = boxz.x1;
	page.frames[frameIndex].y1 = boxz.y1;
	page.frames[frameIndex].x2 = boxz.x2;
	page.frames[frameIndex].y2 = boxz.y2;
	console.dir(page.frames);
	console.dir(getPageByIndex(state.editPageIndex));

};

const cancelOneFrame = (index) => {
	console.log('no cancel code here, but reset some changed values');
	showHideFrameEdit(index,'hide');
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, index);
	document.getElementById('frameInputTitle_'+index).value = page.frames[frameIndex].title;
	let boxz = getFrameByIndex(state.ctx.box,index);
	boxz.x1 = page.frames[frameIndex].x1;
	boxz.y1 = page.frames[frameIndex].y1;
	boxz.x2 = page.frames[frameIndex].x2;
	boxz.y2 = page.frames[frameIndex].y2;
	boxz.active = false;
	drawCanvas();
};

const updateMainTitle = () => {
	let mainTitleElement = document.getElementById('mainTitleInput');
	if(mainTitleElement.value!=''||false) {
		console.log(mainTitleElement.value);
		let page = getPageByIndex(state.editPageIndex);
		page.title = mainTitleElement.value;
		renderTemplate('bundleView', pageBundle, 'targetContainer');
	}
};

const editTag = (index,btnIndex) => {
	// console.log("editTag", index, btnIndex);
	let tagBtn = document.getElementById(`tagBtn_${index}_${btnIndex}`);
	// tagBtn.contentEditable = true
	tagBtn.classList.remove('btn-primary');
	tagBtn.classList.add('btn-danger');
	// let popUp = document.getElementById('popupBottom');
	// console.dir(popUp);
	let btn = document.getElementById(`tagBtn_${index}_${btnIndex}`);
	let tooltip = document.getElementById('tagEditToolTip');
	let tagInput = document.getElementById('aktiveTagEdit');
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, index);
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	tagInput.dataset.index = index;
	tagInput.dataset.frameIndex = frameIndex;
	tagInput.dataset.btnIndex = btnIndex;
	tagInput.value = page.frames[frameIndex].tags[btnIndex];
	Popper.createPopper(btn, tooltip, {
		  placement: 'top',
		  modifiers: [
		    {
		      name: 'offset',
		      options: {
		        offset: [0, 8],
		      },
		    },
		  ],
		});
	tooltip.style.display = 'inline-block';
};

const deleteTag = (index,tag) => {
	console.log("deleteTag: ", index, tag);
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, index);
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	if(page.frames[frameIndex].tags||false) {
		let tagIndex = page.frames[frameIndex].tags.indexOf(tag);
		if(tagIndex===-1) return;
		page.frames[frameIndex].tags.splice(tagIndex,1);
		renderTemplate('frameList',pageBundle.pages[arrIndex],"frameListSection");
	} 
};

const addTag = (index) => {
	console.log("addTag", index);
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, index);
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	if(page.frames[frameIndex].tags||false) {
		page.frames[frameIndex].tags.push('new One');
	} else {
		page.frames[frameIndex].tags = ['new One'];
	}
	renderTemplate('frameList',pageBundle.pages[arrIndex],"frameListSection");
};

// const saveTagText = (index, btnIndex) => {
// 	let tagBtn = document.getElementById(`tagBtn_${index}_${btnIndex}`);
// 	if(tagBtn.contentEditable===false) return;
// 	tagBtn.contentEditable = false;
// 	tagBtn.classList.add('btn-primary');
// 	tagBtn.classList.remove('btn-danger');

// 	let page = getPageByIndex(state.editPageIndex);
// 	let frameIndex = getFrameArrayIndex(page.frames, index);
// 	let arrIndex = getPageArrayIndex(state.editPageIndex);
// 	if(page.frames[frameIndex].tags||false) {
// 		page.frames[frameIndex].tags[btnIndex] = tagBtn.innerText.trim();
// 		renderTemplate('frameList',pageBundle.pages[arrIndex],"frameListSection");
// 	} 
// };


const saveTooltipTag = () => {
	let page = getPageByIndex(state.editPageIndex);
	let tagInput = document.getElementById('aktiveTagEdit');
	let frameIndex = tagInput.dataset.frameIndex; 
	let btnIndex = tagInput.dataset.btnIndex;
	let index = tagInput.dataset.index;
	let tagBtn = document.getElementById(`tagBtn_${index}_${btnIndex}`);
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	page.frames[frameIndex].tags[btnIndex] = tagInput.value.trim();
	tagBtn.classList.add('btn-primary');
	tagBtn.classList.remove('btn-danger');
	let tooltip = document.getElementById('tagEditToolTip');
	tooltip.style.display = 'none';
	let listT = document.getElementById('listGroupTooltip');
	listT.style.display = 'none';
	
	renderTemplate('frameList',pageBundle.pages[arrIndex],"frameListSection");
};

const cancelTooltipTag = () => {
	let tagInput = document.getElementById('aktiveTagEdit');
	let tagBtn = document.getElementById(`tagBtn_${tagInput.dataset.index}_${tagInput.dataset.btnIndex}`);
	tagBtn.classList.add('btn-primary');
	tagBtn.classList.remove('btn-danger');

	tagInput.dataset.frameIndex = -1;
	tagInput.dataset.btnIndex = -1;
	tagInput.dataset.index = -1;
	
	let tooltip = document.getElementById('tagEditToolTip');
	tooltip.style.display = 'none';
		let listT = document.getElementById('listGroupTooltip');
	listT.style.display = 'none';
};

const editAllTag = (index) => {
	let tooltip = document.getElementById('tagsMassEditTool');
	let tagsInput = document.getElementById('aktiveMassTagEdit');
	let btn = document.getElementById('frameBtnAllTags_'+index);
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, index);
	let tagStrArr = ''
	if((page.frames[frameIndex].tags||false) && page.frames[frameIndex].tags.length!=0) {
		tagStrArr = page.frames[frameIndex].tags.join(",");
	}
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	tagsInput.dataset.index = index;
	tagsInput.dataset.frameIndex = frameIndex;
	
	tagsInput.value = tagStrArr;
	Popper.createPopper(btn, tooltip, {
		  placement: 'top',
		  modifiers: [
		    {
		      name: 'offset',
		      options: {
		        offset: [0, 8],
		      },
		    },
		  ],
		});
	tooltip.style.display = 'inline-block';
	let listT = document.getElementById('listGroupTooltip');
	listT.style.display = 'none';
};


const saveTooltipMassTag = () => {
	let page = getPageByIndex(state.editPageIndex);
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	let tagsInput = document.getElementById('aktiveMassTagEdit');
	let frameIndex = tagsInput.dataset.frameIndex; 
	let index = tagsInput.dataset.index;
	let tagBtn = document.getElementById('frameBtnAllTags_'+index);
	let splitTags = tagsInput.value.split(',');
	let newTagList = [];
	for(let s=0,sl=splitTags.length; s < sl; s++) {
		// tagsInput.value.trim()
		newTagList.push(splitTags[s].trim());
	}
	
	page.frames[frameIndex].tags = newTagList;
	
	let tooltip = document.getElementById('tagsMassEditTool');
	tooltip.style.display = 'none';
	let listT = document.getElementById('listGroupTooltip');
	listT.style.display = 'none';

	renderTemplate('frameList',pageBundle.pages[arrIndex],"frameListSection");

};

const cancelTooltipMassTag = () => {
	let tagInput = document.getElementById('aktiveMassTagEdit');
	let tagBtn = document.getElementById('frameBtnAllTags_'+tagInput.dataset.index);
	// tagBtn.classList.add('btn-primary');
	// tagBtn.classList.remove('btn-danger');

	tagInput.dataset.frameIndex = -1;
	tagInput.dataset.index = -1;
	
	let tooltip = document.getElementById('tagsMassEditTool');
	tooltip.style.display = 'none';
	let listT = document.getElementById('listGroupTooltip');
	listT.style.display = 'none';
};


const updateListGroupTooltip = (resultList,target) => {
	let listT = document.getElementById('listGroupTooltip');
	let tagInput = document.getElementById(target);
	if(listT.style.display==='none') {
		listT.style.display = 'inline-block';
	}
	console.dir(resultList);
	// resultList = {result:['Spacemarines','Orden','Text1',"more Text to test",'hello world']};
	renderTemplate('searchResult',resultList,'listGroupTooltip');
	
	Popper.createPopper(tagInput, listT, {
		  placement: 'bottom',
		  modifiers: [
		    {
		      name: 'offset',
		      options: {
		        offset: [15, 15],
		      },
		    },
		  ],
		});
};

const selectResult = (selStr,type) => {
	console.log(`Selected Search ${selStr}`);
	let tagInput = document.getElementById('aktiveTagEdit');
	tagInput.value = selStr;
	saveTooltipTag();
};


const singleTagSearch = () => {
	let tagInput = document.getElementById('aktiveTagEdit');
	if(tagInput.value.length<=2) return;
	let resultList = tagBackSearch(tagInput.value);
	// console.log(resultList);
	
	updateListGroupTooltip({result:resultList,type:'single'},'aktiveTagEdit')
};


const backToLastImage = () => {
	if(state.lastImageIndex||false) {
		document.getElementById('imageIndex_'+state.lastImageIndex).scrollIntoView({ left: 0, block: 'start', behavior: 'smooth' });
	}
};

const nextImageInList = () => {
	if(state.lastImageIndex||false) {
		let lastFrame = parseInt(state.lastImageIndex);
		let pageData = getPageByIndex(lastFrame);
		if((pageData||false) && (pageData.index||false)) {
			editPage( getNextIndexInPages(pageData.index) );
		}
	}
};

const prevImageInList = () => {
	if(state.lastImageIndex||false) {
		let lastFrame = parseInt(state.lastImageIndex);
		let pageData = getPageByIndex(lastFrame);
		if((pageData||false) && (pageData.index||false)) {
			editPage( getPrevIndexInPages(pageData.index) );
		}
	}
};

const deleteFrameFromList = (index) => {
 	let delBtn = document.getElementById("areYouSureBtn_"+index);
 	if(delBtn.style.display === 'none') {
 		delBtn.style.display = 'inline-block';
 	} else {
 		delBtn.style.display = 'none';
 	}
};

const realDeleteFrameFromList = (index) => {
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, index);
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	page.frames.splice(frameIndex,1);
	
	renderTemplate('frameList',pageBundle.pages[arrIndex],"frameListSection");
	framesToBoxes();
	drawCanvas();
};

const showHideMultiSelectSaveCancel = (mode) => {
	let saveBtn = document.getElementById('saveBtnMultiSelect');
	let cancelBtn = document.getElementById('cancelBtnMultiSelect');
	if(mode==='hide') {
		saveBtn.style.display='none';
		cancelBtn.style.display='none';
	} else if(mode==='show') {
		saveBtn.style.display='inline-block';
		cancelBtn.style.display='inline-block';
	}
};

const toggleMultiSelect = () => {
	let toggleBtn = document.getElementById('toggleBtnMultiSelect');
	if(state.ctx.multiselect) {
		state.ctx.multiselect = false;
		toggleBtn.classList.remove('btn-danger');
		toggleBtn.classList.add('btn-primary');
	} else {
		state.ctx.multiselect = true;
		toggleBtn.classList.remove('btn-primary');
		toggleBtn.classList.add('btn-danger');
	}
};


const moreFramesWereEdited = () => {
	showHideMultiSelectSaveCancel('show');
};


const saveMultiSelectedBox = () => {
	let page = getPageByIndex(state.editPageIndex);
	for(let b=0,bl=state.ctx.box.length; b < bl; b++) {
		if(state.ctx.box[b].active) {
			page.frames[b].x1 = state.ctx.box[b].x1;
			page.frames[b].x2 = state.ctx.box[b].x2;
			page.frames[b].y1 = state.ctx.box[b].y1;
			page.frames[b].y2 = state.ctx.box[b].y2;
			state.ctx.box[b].active = false;
		}
	}
	showHideMultiSelectSaveCancel('hide');
	toggleMultiSelect();
	drawCanvas();
};


const cancelMultiSelectedBox = () => {
	let page = getPageByIndex(state.editPageIndex);
	for(let b=0,bl=state.ctx.box.length; b < bl; b++) {
		if(state.ctx.box[b].active) {
			state.ctx.box[b].x1 = page.frames[b].x1;
			state.ctx.box[b].x2 = page.frames[b].x2;
			state.ctx.box[b].y1 = page.frames[b].y1;
			state.ctx.box[b].y2 = page.frames[b].y2;
			state.ctx.box[b].active = false;
		}
	}
	showHideMultiSelectSaveCancel('hide');
	toggleMultiSelect();
	drawCanvas();
};

const toggleLinkMenu = () => {
	let container = document.getElementById('linkMenuContainer');
	if(container.style.display==='none') {
		container.style.display='inline';
		toggleViewOfAllLinkBtn('show');
	} else {
		container.style.display='none';
		toggleViewOfAllLinkBtn('hide');
	}
};

const toggleViewOfAllLinkBtn = (showhide) => {
	let mode = (showhide==='show')? 'inline' : 'none';
	let alBtn = document.querySelectorAll('.editLinkBtn');
	for(let b=0,bl=alBtn.length; b < bl; b++) {
		alBtn[b].style.display = mode;
		// toggleCheckLinkBtn(alBtn[b],true);
	}
	getLinksFromPageBundle();
	setBtnStatusByMode();
};

const getLinksFromPageBundle = () => {
	if(state.editPageIndex===0) return {err:'no state.editPageIndex'};
	let page = getPageByIndex(state.editPageIndex);
	let frames = page.frames;
	state.links = [];
	for(let f=0,fl=frames.length; f < fl; f++) {
		if(frames[f].links||false) {
			state.links[f] = frames[f].links;
		} else {
			state.links[f] = {type:null,source:false,target:{source:"0-0"}};
		}
	}
};

const setLinksFromStateToBundle = () => {
	let page = getPageByIndex(state.editPageIndex);
	let frames = page.frames;
	for(let f=0,fl=state.links.length; f < fl; f++) {
		if(state.links[f].type!='null'||false) {
			frames[f].links = state.links[f];
		} else {
			frames[f].links = null;
		}
	}
}

const setBtnStatusByMode = () => {
	// if(!(state.linksMode||false)) return;
	// let status = state.linksMode;
	let links = state.links;
	for(let s=0,sl=links.length; s < sl; s++) {
		if(!(links[s]||false)) continue;
		if(links[s].type==='source') {
			setCheckLinkBtn('source',s);
		} else if(links[s].type==='target') {
			setCheckLinkBtn('target',s);
		}

	// 	let theBtn = document.getElementById("checkToggleLinkBtn_"+s);
	// 	if(links.type===status) {
	// 		theBtn.classList.remove('btn-primary');
	// 		theBtn.classList.add('btn-danger');
	// 		theBtn.firstElementChild.classList.remove('fa-square');
	// 		theBtn.firstElementChild.classList.add('fa-check-square');
	// 	} else {
	// 		theBtn.classList.add('btn-primary');
	// 		theBtn.classList.remove('btn-danger');
	// 		theBtn.firstElementChild.classList.add('fa-square');
	// 		theBtn.firstElementChild.classList.remove('fa-check-square');
	// 	}

	}
};

// const linkTargetToggleBtn = (toActive) => {
// 	let source = document.getElementById('linkTargetBtn_source');
// 	let target = document.getElementById('linkTargetBtn_target');
// 	if(toActive==='source') {
// 		source.classList.remove('btn-light');
// 		source.classList.add('btn-danger');
// 		target.classList.add('btn-light');
// 		target.classList.remove('btn-danger');
// 		state.linksMode = 'source';
// 	} else if(toActive==='target') {
// 		target.classList.remove('btn-light');
// 		target.classList.add('btn-danger');
// 		source.classList.add('btn-light');
// 		source.classList.remove('btn-danger');
// 		state.linksMode = 'target';
// 	}
// 	setBtnStatusByMode();
// };

const toggleCheckLinkBtn = (linktype,btnIndex) => {
	setCheckLinkBtn(linktype,btnIndex,true);
	// console.log(`Target ${linktype} | Index ${btnIndex}`);
	// let theBtn = {};
	
	// if(linktype==='source') {
	// 	state.links[btnIndex]
	// 	console.log('inside source here, came from list');
	// 	theBtn = document.getElementById('checkToggleLinkSourceBtn_'+btnIndex);
	// 	if(theBtn.className.indexOf('btn-primary')===-1) {
	// 		theBtn.classList.add('btn-primary');
	// 		theBtn.classList.remove('btn-danger');
	// 		theBtn.firstElementChild.classList.add('fa-square');
	// 		theBtn.firstElementChild.classList.remove('fa-check-square');
	// 		state.links[btnIndex].type = null;
	// 		state.links[btnIndex].source = false;			
	// 	} else {
	// 		theBtn.classList.remove('btn-primary');
	// 		theBtn.classList.add('btn-danger');
	// 		theBtn.firstElementChild.classList.remove('fa-square');
	// 		theBtn.firstElementChild.classList.add('fa-check-square');
	// 		state.links[btnIndex].type = 'source';
	// 		state.links[btnIndex].source = true;
	// 	}
	// } else if(linktype==='target') {
	// 	theBtn = document.getElementById('checkToggleLinkTargetBtn_'+btnIndex);
	// 	if(theBtn.className.indexOf('btn-warning')===-1) {
	// 		theBtn.classList.add('btn-warning');
	// 		theBtn.classList.remove('btn-danger');
	// 		theBtn.firstElementChild.classList.add('fa-square');
	// 		theBtn.firstElementChild.classList.remove('fa-check-square');
	// 		state.links[btnIndex].type = null;
	// 	} else {
	// 		theBtn.classList.remove('btn-warning');
	// 		theBtn.classList.add('btn-danger');
	// 		theBtn.firstElementChild.classList.remove('fa-square');
	// 		theBtn.firstElementChild.classList.add('fa-check-square');		
	// 		state.links[btnIndex].type = 'target';
	// 		state.links[btnIndex].target = {source:'23-42'};
	// 	} 		
	// }
};

const setCheckLinkBtn = (type,index,toggle) => {
	let theBtn = {};
	let active = -1;
	toggle = (toggle||false)? true : false ;
	if(type==='source') {
		theBtn = document.getElementById('checkToggleLinkSourceBtn_'+index);
		active = (theBtn.className.indexOf('btn-primary')===-1)? true : false ;
		if(toggle && active===true) {
			theBtn.classList.add('btn-primary');
			theBtn.classList.remove('btn-danger');
			theBtn.firstElementChild.classList.add('fa-square');
			theBtn.firstElementChild.classList.remove('fa-check-square');
			state.links[index].type = null;
			state.links[index].source = false;
		} else if(toggle || active===false) {
			theBtn.classList.remove('btn-primary');
			theBtn.classList.add('btn-danger');
			theBtn.firstElementChild.classList.remove('fa-square');
			theBtn.firstElementChild.classList.add('fa-check-square');
			state.links[index].type = 'source';
			state.links[index].source = true;
		}
	} else if(type==='target') {
		theBtn = document.getElementById('checkToggleLinkTargetBtn_'+index);
		active = (theBtn.className.indexOf('btn-warning')===-1)? true : false ;
		if(toggle && active===true) {
			theBtn.classList.add('btn-warning');
			theBtn.classList.remove('btn-danger');
			theBtn.firstElementChild.classList.add('fa-square');
			theBtn.firstElementChild.classList.remove('fa-check-square');
			state.links[index].type = null;
		} else if(toggle || active===false) {
			theBtn.classList.remove('btn-warning');
			theBtn.classList.add('btn-danger');
			theBtn.firstElementChild.classList.remove('fa-square');
			theBtn.firstElementChild.classList.add('fa-check-square');		
			state.links[index].type = 'target';
			state.links[index].target = {source:'23-42'};
		}
	}
};

const saveLinkStatus = () => {
	// for(let l=0,ll=state.links.length; l < ll; l++) {
	// 	let lBtn = document.getElementById('checkToggleLinkBtn_'+l);
	// 	if(lBtn.className.indexOf('danger')>-1) {
	// 		state.links[l].type = state.linksMode;
	// 	} else if(state.linksMode==='source') {
	// 		state.links[l].type = 'none';
	// 	}
	// }
	setLinksFromStateToBundle();
}