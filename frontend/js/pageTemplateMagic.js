// after helperMagic.js became near 500 lines of code i decide to start a new file of magic
// 
// the template handling for the frames on a page should be a easy way to copy it to an other page Bundle
// part of the project will be tested with a 80 issue/pageBundle series. so start early with good tooling ... 
// 

// load save superagent suff

const getListOfFrameTemplates = () => {
  superagent
   .get('/json/template/list')
   .then( res => {
      let advList = [];
      let flist = res.body.filelist;
      for(let f=0,fl=flist.length; f < fl; f++) {
      	let cutSlp = flist[f].replace('.json','').split("_");
      	advList.push({full:flist[f],name:cutSlp[1],number:parseInt(cutSlp[0])});
      }
      advList.sort( (a,b) => { return (a.number-b.number); } );
      renderTemplate('frameTemplateSelect', {filelist:advList}, 'templeteListSelectTarget');
   })
   .catch( err => {
   		console.log(err.message);
      console.dir(err.response);
   });
};

const getFrameTemplate = (templName) => {
  if(templName==="-1") return;
  superagent
   .get('/json/template/'+templName)
   .then( res => {
      drawTemplateOntoPage(res.body);
   })
   .catch( err => {
   		console.log(err.message);
      console.dir(err.response);
   });
};

const toggleTemplateMenu = () => {
	let menuElm = document.getElementById('templeteMenuContainer');
	if(menuElm.style.display==='none') {
		menuElm.style.display='flex';
	} else {
		menuElm.style.display='none';
	}
};

const newSelectedTemplate = () => {
	let tplInput = document.getElementById('newTempleteName');
	if(tplInput.value===''||tplInput.value.length<=3) return;
	console.log(tplInput.value);
	
};

const loadSelectedTemplate = () => {
	let selectedTpl = document.getElementById('templeteListSelectTarget');
	getFrameTemplate(selectedTpl.options[selectedTpl.selectedIndex].value);
};

const drawTemplateOntoPage = (tempObj) => {
	console.dir(tempObj);
	let page = getPageByIndex(state.editPageIndex);
	page.frames = tempObj;
	editPage(state.lastImageIndex);
};