let canvas = {};
let ctx = {};
const initCanvas = (canvasID) => {
	if(canvasID||false) {
		canvas = document.getElementById(canvasID);
	} else {
		canvas = document.querySelector('canvas');
	}
	ctx = canvas.getContext('2d');
};

const toggleWindow = (elementId,forceView) => {
	let editContainer = document.getElementById(elementId);
	if(!(editContainer||false)){
		console.log("no Window here !1!!11!1!!");
	} else if(editContainer.className.indexOf('d-none')===-1) {
		editContainer.className += " d-none";
	} else if(!forceView||false) {
		editContainer.className = editContainer.className.replace(' d-none',"");
	}
};

const framesToBoxes = () => {
	let arrIndex = getPageArrayIndex(state.editPageIndex);
	let tBox = [];
	for(let f=0,fl=pageBundle.pages[arrIndex].frames.length; f < fl; f++) {
		let tf = pageBundle.pages[arrIndex].frames[f];
		tBox.push({x1:tf.x1,y1:tf.y1,x2:tf.x2,y2:tf.y2,index:tf.index,active:false});
	}
	if(tBox.length>0) {
		state.ctx.box = tBox;
	}
};

const pointToSizeBox = (x1,y1,x2,y2,fac) => {
	ctx.strokeRect( x1/fac, y1/fac, (x2-x1)/fac, (y2-y1)/fac );
};

const drawCanvas = () => {
	let heightFac = state.ctx.img.height / canvas.height;
	let widthFac = state.ctx.img.width / canvas.width;
	let diFa = Math.max(heightFac, widthFac);
	// let diFa = state.ctx.img.height / canvas.height;
	let nW = state.ctx.img.width / diFa;
	let nH = state.ctx.img.height / diFa;

	state.ctx.zoomFac = diFa;

	// clear before draw new
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	// our main image
	ctx.drawImage(state.ctx.img,0,0,nW,nH);

	// and now the boxes
	ctx.beginPath();
	ctx.lineWidth = 3;
	for(let b=0,bc=state.ctx.box.length; b < bc; b++) {
		let tb = state.ctx.box[b];
		ctx.strokeStyle = 'blue';
		if(tb.active) {
			ctx.strokeStyle = 'yellow';
			ctx.lineWidth = 1;
			let pX = (tb.x2-tb.x1)*.2;
			let pY = (tb.y2-tb.y1)*.2;
			let fXY = ( (tb.x2-tb.x1) + (tb.y2-tb.y1) )*.05;

			pointToSizeBox(tb.x1, tb.y1+fXY, tb.x1+fXY, tb.y2-fXY, diFa);
			pointToSizeBox(tb.x2-fXY, tb.y1+fXY, tb.x2, tb.y2-fXY, diFa);
			pointToSizeBox(tb.x1+fXY, tb.y1, tb.x2-fXY, tb.y1+fXY, diFa);
			pointToSizeBox(tb.x1+fXY, tb.y2-fXY, tb.x2-fXY, tb.y2, diFa);
			ctx.lineWidth = 3;
			ctx.strokeStyle = 'red';
		}
		// ctx.strokeRect(tb.x1/diFa,tb.y1/diFa,(tb.x2-tb.x1)/diFa,(tb.y2-tb.y1)/diFa);
		pointToSizeBox(tb.x1, tb.y1, tb.x2, tb.y2, diFa);
	}
	ctx.stroke();
	ctx.closePath();
};

const editPage = (pageIndex) => {
	let pageData = getPageByIndex(pageIndex);
	state.lastImageIndex = pageIndex;
	fillEditPageData(pageData);
	// rebuild state
	framesToBoxes();
	let targetDiv = document.getElementById('canvasDiv');
	// let editContainer = document.getElementById('editWindow');
	canvas.width = targetDiv.offsetWidth;
	canvas.height = window.innerHeight;
	// toggleWindow('editWindow',true);
	let imgSrc = document.getElementById('imageIndex_'+pageIndex);
	state.ctx.img = new Image();
	state.ctx.img.src = imgSrc.src;
	state.ctx.img.onload = function() {
		drawCanvas();
	};
	window.scrollTo({ top: 0, behavior: 'smooth' });
};


const editFrameDimensions = (frameIndex) => {
	enableCloneButton();

	if(state.ctx.multiselect) {
		state.ctx.box[frameIndex].active = (state.ctx.box[frameIndex].active)? false : true;
		let btn = document.getElementById('frameBtnDimensions_'+frameIndex);
		btn.classList.toggle('btn-info');
		btn.classList.toggle('btn-danger');
	} else {
		state.ctx.activeBox = frameIndex;
		for(let b=0,bl=state.ctx.box.length; b < bl; b++) {
			let singleBtn = document.getElementById('frameBtnDimensions_'+b);
			if(singleBtn.classList.value.indexOf('btn-danger')>-1) {
				singleBtn.classList.remove('btn-danger');
				singleBtn.classList.add('btn-info');
			}
			if(frameIndex===b) {
				state.ctx.box[b].active = true;
				singleBtn.classList.add('btn-danger');
				singleBtn.classList.remove('btn-info');
			} else {
				state.ctx.box[b].active = false;
			}
		}
	}
	drawCanvas();
};



// mouse event magic
const getMousePos = (evt) => {
  let rect = canvas.getBoundingClientRect(); // abs. size of element
  let scaleX = canvas.width / rect.width;    // relationship bitmap vs. element for X
  let scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y

  return {
    x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
    y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
  }
}
const doMagicStart = (e) => {
	let pos = getMousePos(e);
	if(state.ctx.activeBox||false) {
		let box = state.ctx.box[state.ctx.activeBox];
		let fac = state.ctx.zoomFac;
		let x1 = box.x1/fac;
		let y1 = box.y1/fac;
		let x2 = box.x2/fac;
		let y2 = box.y2/fac;

		if(pos.x>x1&&pos.x<x2&&pos.y>y1&&pos.y<y2) {
			// calc value of dist from the edges
			let pXY = ( (x2-x1)+(y2-y1) )*.05;
			// we hit the box save position for movement
			state.ctx.lastMousePos = pos;
			if(pos.x<=x1+pXY) {
			// check left side and they corners
				if(pos.y<=y1+pXY) {
					state.ctx.boxClicked = 'upleft';
				} else if(pos.y>=y2-pXY) {
					state.ctx.boxClicked = 'lowleft';
				} else {
					state.ctx.boxClicked = 'left';
				}
			} else if(pos.x>=x2-pXY) {
			// check right side and corners
				if(pos.y<=y1+pXY) {
					state.ctx.boxClicked = 'upright';
				} else if(pos.y>=y2-pXY) {
					state.ctx.boxClicked = 'lowright';
				} else {
					state.ctx.boxClicked = 'right';
				}
			} else if(pos.y<=y1+pXY) {
				state.ctx.boxClicked = 'up';
			} else if(pos.y>=y2-pXY) {
				state.ctx.boxClicked = 'down';
			} else {
				state.ctx.boxClicked = 'middle';
			}
		}
	} else if(state.ctx.multiselect||false) {
		// min max pos from list of active boxes
		let fac = state.ctx.zoomFac;
		let mmPos = getActiveBoxMinMaxPos(state.ctx.box,fac);
		if( pos.x>mmPos.min.x && pos.x<mmPos.max.x && pos.y>mmPos.min.y && pos.y<mmPos.max.y) {
			// we hit the box save position for movement
			state.ctx.lastMousePos = pos;
			state.ctx.boxClicked = 'multibox';
		}
	}
};

const doMagicEnd = (e) => {
	state.ctx.boxClicked = null;
	state.ctx.lastMousePos = null;
};

const doBlackMagic = (e) => {
	console.log('what do you think you a doing ....');
	state.ctx.boxClicked = null;
	state.ctx.lastMousePos = null;
};
const speed = 4;

const doMoveMagic = (e) => {
	if( !(state.ctx.lastMousePos || false) ) return;
	let mouseNow = getMousePos(e);
	let xDiff = mouseNow.x - state.ctx.lastMousePos.x;
	let yDiff = mouseNow.y - state.ctx.lastMousePos.y;
	if( Math.abs(xDiff)<=0 && Math.abs(yDiff)<=0 ) return;
	xDiff*=speed;
	yDiff*=speed;

	if( (state.ctx.boxClicked||false)) {
		let box = state.ctx.box[state.ctx.activeBox];
		if(state.ctx.boxClicked!='multibox') {
			someFrameWasEdited(state.ctx.activeBox);
		} else {
			moreFramesWereEdited();
		}

		if(state.ctx.boxClicked==='left') {
			box.x1 += xDiff;
		} else if(state.ctx.boxClicked==='right') {
			box.x2 += xDiff;
		} else if(state.ctx.boxClicked==='up') {
			box.y1 += yDiff;
		} else if(state.ctx.boxClicked==='down') {
			box.y2 += yDiff;
		} else if(state.ctx.boxClicked==='middle') {
			box.x1 += xDiff;
			box.x2 += xDiff;
			box.y1 += yDiff;
			box.y2 += yDiff;
		} else if(state.ctx.boxClicked==='upleft') {
			box.x1 += xDiff;
			box.y1 += yDiff;
		} else if(state.ctx.boxClicked==='lowleft') {
			box.x1 += xDiff;
			box.y2 += yDiff;
		} else if(state.ctx.boxClicked==='upright') {
			box.x2 += xDiff;
			box.y1 += yDiff;
		} else if(state.ctx.boxClicked==='lowright') {
			box.x2 += xDiff;
			box.y2 += yDiff;
		} else if(state.ctx.boxClicked==='multibox') {
			for(let b=0,bl=state.ctx.box.length; b < bl; b++) {
				if(state.ctx.box[b].active) {
					state.ctx.box[b].x1 += xDiff;
					state.ctx.box[b].x2 += xDiff;
					state.ctx.box[b].y1 += yDiff;
					state.ctx.box[b].y2 += yDiff;
				}
			}
		}
		state.ctx.lastMousePos = mouseNow;
		// console.log(`BOX x1:${box.x1}|y1:${box.y1}|x2:${box.x2}|y2:${box.y2} - xDiff:${xDiff}|yDiff:${yDiff}`);
		drawCanvas();
	}
};

const enableCloneButton = () => {
	let btn = document.getElementById('cloneFrameBtn');
	btn.classList.remove('btn-light');
	btn.classList.add('btn-danger');
};

const cloneOneFrame = () => {
	if(document.getElementById('cloneFrameBtn').className.indexOf('btn-light')>-1) return;
	let cloneBox = state.ctx.box[state.ctx.activeBox]; // Object.assign({},state.ctx.box[state.ctx.activeBox]);
	cloneBox.active = false;
	let page = getPageByIndex(state.editPageIndex);
	let frameIndex = getFrameArrayIndex(page.frames, cloneBox.index);	
	console.dir(page.frames[frameIndex]);
	let newBoxIndex = addFrameToPage(page.frames[frameIndex]);
};


const startAlignGroup = () => {
	if(!state.ctx.multiselect) return;
	let left = 0;
	let right = 0;
	let count = 0;
	for(let b=0,bl=state.ctx.box.length; b < bl; b++) {
		if(state.ctx.box[b].active) {
			left += state.ctx.box[b].x1;
			right += state.ctx.box[b].x2;
			count++;
		}
	}
	console.log(`LeftNew: ${right/count} == RightNew: ${left/count}`);
	for(let b=0,bl=state.ctx.box.length; b < bl; b++) {
		if(state.ctx.box[b].active) {
			state.ctx.box[b].x1 = left/count;
			state.ctx.box[b].x2 = right/count;
		}
	}
	moreFramesWereEdited();
	drawCanvas();
}

const setAlignGroup = (side, move) => {
	for(let b=0,bl=state.ctx.box.length; b < bl; b++) {
		if(state.ctx.box[b].active) {
			if(side==='left' && move==='left') {
				state.ctx.box[b].x1 -= speed;
			} else if(side==='left' && move==='right') {
				state.ctx.box[b].x1 += speed;
			} else if(side==='right' && move==='left') {
				state.ctx.box[b].x2 -= speed;
			} else if(side==='right' && move==='right') {
				state.ctx.box[b].x2 += speed;
			}
		}
	}
	moreFramesWereEdited();
	drawCanvas();
};